cloud_key_store
=====
An OTP application to store key values in AWS DynamoDB.


Build
-----
    $ rebar3 compile
    $ rebar3 release
    $ ./_build/default/rel/cloud_key_store/bin/cloud_key_store console


vm.args
=======

__start_mode__ (an optional argument, default value is server_mode).
<br/>The Application can be started in two modes
- __client_mode:__ In this mode cloud_key_store service will not be started and user can only use this application as a client to send request to the server.
Use this mode if the service is running on a remote server.

- __server_mode:__ In this mode cloud_key_store service will be started and clients can be also be started.

In both the modes a default client is started. Please note in case the server is not reachable at the time of starting the application then the default client will show some error messages.
You can ignore these messages, make sure the service is running and correct IP and PORT are configured and then can always start a new client using start_client API.

__aws_access_key_id__
<br/>This is a required argument can only be ignored if start_mode is client_mode.

__aws_secret_key__ 
<br/>This is a required argument can only be ignored if start_mode is client_mode.

__aws_region__ 
<br/>This is a required argument can only be ignored if start_mode is client_mode.

__aws_ddb_table_name__ 
<br/>This is a required argument can only be ignored if start_mode is client_mode.



sys.config
==========
__server_port__
<br/>The port where service is running, used by both client and server

__server_host__
<br/>IP or Hostname of the server where service is running, only used by client.

Docker
=======
```
docker run --rm -it --env mode="server_mode" --env aws_id="AWD-ID" --env aws_secret="SECRET" --env aws_region="us-east-1" keystore:latest
```



Client-API's 
============
__module: cloud_key_store_client__
<br/>Upon starting the application, one client is started by default, use below API's to store and get key values using default client

__store(Key, Value).__

__get(Key).__

examples:
```
cloud_key_store_client:store("k1", "This is data piece 1").
cloud_key_store_client:get("k1").
```

Use below API's to start a new client and store & get keys 

__start_client(clientName).__
<br/>here, clientName is an atom

__store(clientName, Key, Value).__

__get(clientName, Key).__

examples:
```
cloud_key_store_client:start_client(client_1).
cloud_key_store_client:get(client_1, "k1").
cloud_key_store_client:store(client_1, "k1", "Data overwritten by client_1").
```


Helper API's
==========
__cloud_key_store_utils:generate_kb_data(integer_number)__
<br/>This API can be used to generate given number of Kilobytes of data.

example:
```
Below call will store 4KB of data in key "k1".
cloud_key_store_client:store("k1", cloud_key_store_utils:generate_kb_data(4)).
```



