FROM erlang:21.3.8.7-alpine

RUN apk update && \
    apk add \
    bash \
    git \
    vim

RUN git clone https://gitlab.com/anmolsingh008/cloud_key_store.git

ENV aws_region="us-east-1"
ENV aws_id="aws_id"
ENV aws_secret="aws_secret"
ENV mode="client_mode"

WORKDIR cloud_key_store

RUN ./rebar3 release

WORKDIR _build/default/rel/cloud_key_store/

CMD ["sh", "-c", "./bin/cloud_key_store console -start_mode $mode -aws_access_key_id $aws_id -aws_secret_key $aws_secret -aws_region $aws_region"]
