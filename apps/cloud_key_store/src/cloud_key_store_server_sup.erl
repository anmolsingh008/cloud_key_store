%%%-------------------------------------------------------------------
%%% @author esianmo
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 20. Nov 2018 11:28 AM
%%%-------------------------------------------------------------------
-module(cloud_key_store_server_sup).
-author("esianmo").

-include("cloud_key_store.hrl").
-behaviour(supervisor).

%% API
-export([start_link/0, start_child/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the supervisor
%%
%% @end
%%--------------------------------------------------------------------
-spec(start_link() ->
  {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
  cloud_key_store_ddb:setup(),
  supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a supervisor is started using supervisor:start_link/[2,3],
%% this function is called by the new process to find out about
%% restart strategy, maximum restart frequency and child
%% specifications.
%%
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
  {ok, {SupFlags :: {RestartStrategy :: supervisor:strategy(),
    MaxR :: non_neg_integer(), MaxT :: non_neg_integer()},
    [ChildSpec :: supervisor:child_spec()]
  }} |
  ignore |
  {error, Reason :: term()}).
init([]) ->
  {ok, Port} = application:get_env(?APP, server_port),
%%  {ok, ListenSocket} = gen_tcp:listen(Port, [binary, {active, false}]),
  {ok, ListenSocket} = gen_tcp:listen(Port, [binary, {active, false},{packet, 4}]),
  spawn_link(fun start_listeners/0),
  {ok, {{simple_one_for_one, 10, 3600}, [{cloud_key_store_server, {cloud_key_store_server, start_link, [ListenSocket]},
    temporary, 1000, worker, [cloud_key_store_server]}]}}.

start_child() ->
  supervisor:start_child(?MODULE, []).

%%%===================================================================
%%% Internal functions
%%%===================================================================
start_listeners() ->
  {ok, ListenerPoolSize} = application:get_env(listener_pool_size),
  [start_child() || _ <- lists:seq(1, ListenerPoolSize)].
