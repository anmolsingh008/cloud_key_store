%%%-------------------------------------------------------------------
%% @doc cloud_key_store public API
%% @end
%%%-------------------------------------------------------------------

-module(cloud_key_store_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
    cloud_key_store_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
