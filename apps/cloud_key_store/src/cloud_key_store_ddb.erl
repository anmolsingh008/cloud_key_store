-module(cloud_key_store_ddb).
-author("esianmo").
-include("cloud_key_store.hrl").

-define(ITEM(K, V), [{<<"key">>, K}, {<<"value">>, V}]).
-define(COMPRESS_FIELD, [{?COMPRESS_KEY, {bool, true}}]).
-define(KEY(K), [{<<"key">>, K}]).
-define(TABLE, erlang:list_to_binary(cloud_key_store_utils:get_req_arg(aws_ddb_table_name))).

-export([setup/0, store/3, get/1]).

setup() ->
  ok = configure_keys(),
  ok = create_table(?TABLE).

configure_keys() ->
  AWSAccessKey = cloud_key_store_utils:get_req_arg(aws_access_key_id),
  AWSSecretKey = cloud_key_store_utils:get_req_arg(aws_secret_key),
  AWSRegion = cloud_key_store_utils:get_req_arg(aws_region),
  application:set_env(erlcloud, aws_access_key_id, AWSAccessKey),
  application:set_env(erlcloud, aws_secret_access_key, AWSSecretKey),
  application:set_env(erlcloud, aws_region, AWSRegion).

create_table(TableName) ->
	case erlcloud_ddb2:create_table(TableName,
		[{<<"key">>, s}], <<"key">>, 4, 4, [{sse_specification, {enabled, true}}]) of
	  {ok, _} ->
			ok;
	  {error, {_, <<"Table already exists", _/binary>>}} ->
			ok;
		_ ->
		  throw("Unable to create dynamoDB table")
	end.

store(Key, Value, IsCompression) ->
	try
		case erlcloud_ddb2:put_item(?TABLE, ?ITEM(Key, Value) ++ compress_field(IsCompression)) of
			{ok, _} ->
				ok;
			{error, _Error} ->
				error
		end
	catch
		_:_ ->
			error
	end.

get(Key) ->
	try
		case erlcloud_ddb2:get_item(?TABLE, ?KEY(Key)) of
			{ok, []} ->
				{not_found, undefined};
			{ok, Item} ->
				{ok, Item};
			{error, _} ->
				{internal, undefined}
		end
	catch
		_:_ ->
			{internal, undefined}
	end.

compress_field(true) ->
	?COMPRESS_FIELD;
compress_field(_) ->
	[].
