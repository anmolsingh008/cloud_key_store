%%%-------------------------------------------------------------------
%%% @author esianmo
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 20. Nov 2018 2:01 PM
%%%-------------------------------------------------------------------
-module(cloud_key_store_client).
-author("esianmo").
-include("cloud_key_store.hrl").
-include("kv_pb.hrl").
-behaviour(gen_server).

-define(TIME_OUT, 5000).

%% API
-export([start_link/0, start_link/1, stop/0, store/2, get/1]).
-export([start_client/1, stop/1, store/3, get/2]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(DEFAULT_CLIENT_NAME, ?MODULE).

-record(state, {socket}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
start_link() ->
  start_link(?DEFAULT_CLIENT_NAME).

start_client(Name) ->
  cloud_key_store_client_sup:start_child(Name).

start_link(ClientName) when is_atom(ClientName)->
  gen_server:start_link({local, ClientName}, ?MODULE, [], []);
start_link(_) ->
  use_atom_as_client_name.

stop() ->
  stop(?DEFAULT_CLIENT_NAME).

stop(Client) ->
  gen_server:stop(Client).

store(Key, Value) ->
  store(?DEFAULT_CLIENT_NAME, Key, Value).

store(Client, Key, Value) ->
  Packet = cloud_key_store_utils:create_set_request_envelop(Key, Value),
  gen_server:call(Client, {send, Packet}).

get(Key) ->
  get(?DEFAULT_CLIENT_NAME, Key).

get(Client, Key) ->
  Packet = cloud_key_store_utils:create_get_request_envelop(Key),
  gen_server:call(Client, {send, Packet}).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
  {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term()} | ignore).
init([]) ->
  {ok, Port} = application:get_env(?APP, server_port),
  {ok, Host} = application:get_env(?APP, server_host),
  {ok, Socket} = gen_tcp:connect(Host, Port, [binary, {packet, 4}]),
  {ok, #state{socket = Socket}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
  {reply, Reply :: term(), NewState :: #state{}} |
  {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_call({send, Packet}, _From, #state{socket = Socket} = State) ->
  Response = send_and_receive(Socket, Packet),
  {reply, Response, State};
handle_call(_Request, _From, State) ->
  {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_cast(_Request, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, #state{socket = Socket}) ->
  gen_tcp:close(Socket),
  erlang:display("Client connection terminated"),
  ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
  {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
send_and_receive(Socket, Packet) ->
  ok = gen_tcp:send(Socket, Packet),
  receive
    {tcp, Socket, Response} ->
      cloud_key_store_utils:process_message(Response)
  after
    ?TIME_OUT ->
      request_timed_out
end.
