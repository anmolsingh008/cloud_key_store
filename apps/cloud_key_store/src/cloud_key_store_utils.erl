-module(cloud_key_store_utils).
-author("esianmo").
-include("cloud_key_store.hrl").
-include("kv_pb.hrl").
-compile([export_all]).

-define(DDB_SIZE_LIMIT, 400000).

get_req_arg(Name) ->
	case init:get_argument(Name) of
    {ok, [[Val]]} ->
      Val;
    _ ->
      throw("Required Argument missing")
  end.

get_arg(Name) ->
	case init:get_argument(Name) of
    {ok, [[Val]]} ->
      Val;
    _ ->
      undefined
  end.

process_message(EncodedMsg) ->
  case kv_pb:decode_msg(EncodedMsg, req_envelop) of
    #req_envelop{type = 'set_request_t', set_req = SetReq} ->
      process_set_request(SetReq, do_compression(erlang:term_to_binary(SetReq)));
    #req_envelop{type = 'get_request_t', get_req = GetReq} ->
      process_get_request(GetReq);
    #req_envelop{type = 'set_response_t', set_resp = SetResp} ->
      process_set_response(SetResp);
    #req_envelop{type = 'get_response_t', get_resp = GetResp} ->
      process_get_response(GetResp)
  end.

process_set_request(#set_request{req = #data{key = _Key, value = Value} = Data}, true) ->
  CompressedValue = cloud_key_store_compression:compress(Value),
  process_set_request_1(#set_request{req = Data#data{value = CompressedValue}}, true);
process_set_request(SetRequest, false) ->
  process_set_request_1(SetRequest, false).

process_set_request_1(#set_request{req = #data{key = Key, value = Value}}, IsCompressed) ->
  Response = case cloud_key_store_ddb:store(Key, Value, IsCompressed) of
               ok ->
                 ok;
               error ->
                 internal
             end,
  create_set_response_envelop(Response).

%%process_set_request(#set_request{req = #data{key = Key, value = Value}}, false) ->
%%  Response = case cloud_key_store_ddb:store(Key, Value) of
%%               ok ->
%%                 ok;
%%               error ->
%%                 internal
%%             end,
%%  create_set_response_envelop(Response).

process_get_request(#get_request{key = Key}) ->
  {Response, Req} = case cloud_key_store_ddb:get(Key) of
                      {ok, Item} ->
                        Key = proplists:get_value(<<"key">>, Item),
                        OrigValue = proplists:get_value(<<"value">>, Item),
                        Value = case proplists:get_bool(?COMPRESS_KEY, Item) of
                          true ->
                            cloud_key_store_compression:uncompress(OrigValue);
                          false ->
                            OrigValue
                        end,
                        {ok, #data{key = Key, value = Value}};
                      {Error, _} ->
                        {Error, undefined}
                    end,
  create_get_response_envelop(Response, Req).

%%process_get_request(#get_request{key = Key}) ->
%%  {Response, Req} = case cloud_key_store_ddb:get(Key) of
%%                   {ok,[{<<"key">>, Key}, {<<"value">>, Value}]} ->
%%                     {ok, #data{key = Key, value = Value}};
%%                   {Error, _} ->
%%                     {Error, undefined}
%%                 end,
%%  create_get_response_envelop(Response, Req).

process_set_response(#set_response{error = Resp}) ->
  Resp.

process_get_response(#get_response{error = 'ok', req = #data{key = _Key, value = Value}}) ->
  Value;
process_get_response(#get_response{error = 'not_found'}) ->
  'not_found';
process_get_response(_) ->
  'internal'.

create_set_request_envelop(Key, Value) ->
  Data = #data{key = Key, value = Value},
  SetReq = #set_request{req = Data},
  ReqEnvelop = #req_envelop{
    type = 'set_request_t',
    set_req = SetReq},
  kv_pb:encode_msg(ReqEnvelop).

create_set_response_envelop(Response) ->
  SetResponse = #set_response{error = Response},
  ReqEnvelop = #req_envelop{
    type = 'set_response_t',
    set_resp = SetResponse
  },
  kv_pb:encode_msg(ReqEnvelop).

create_get_request_envelop(Key) ->
  GetReq = #get_request{key = Key},
  ReqEnvelop = #req_envelop{
    type = 'get_request_t',
    get_req = GetReq},
  kv_pb:encode_msg(ReqEnvelop).

create_get_response_envelop(Error, Req) ->
  GetResp = #get_response{error = Error, req = Req},
  ReqEnvelop = #req_envelop{
    type = 'get_response_t',
    get_resp = GetResp
  },
  kv_pb:encode_msg(ReqEnvelop).

do_compression(Req) ->
  ByteSize = byte_size(Req),
  if
    ByteSize > ?DDB_SIZE_LIMIT ->
      erlang:display("Byte-Size:"),
      erlang:display(ByteSize),
      true;
    true ->
      false
  end.

generate_kb_data(N) when is_integer(N) ->
  erlang:iolist_to_binary([ ?ONE_KB_DATA || _ <-lists:seq(1, N)]).

