%%%-------------------------------------------------------------------
%%% @author esianmo
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 23. Nov 2018 11:43 AM
%%%-------------------------------------------------------------------
-module(cloud_key_store_compression).
-author("esianmo").

%% API
-export([compress/1, uncompress/1]).

compress(Data) ->
  erlang:display("Compressing......"),
  zlib:gzip(Data).
%%  zlib:compress(Data).
%%  Z = zlib:open(),
%%  zlib:deflateInit(Z, best_compression,deflated, -15, 9, default),
%%  B1 = zlib:deflate(Z, Data),
%%  B2 = zlib:deflate(Z, <<>>, finish),
%%  zlib:deflateEnd(Z),
%%  list_to_binary([B1, B2]).

uncompress(_Data) ->
  erlang:display("De-compressing......"),
  <<"De-compression not working">>.
%%  zlib:gunzip(Data).
%%  zlib:uncompress(Data).
%%  Z=zlib:open(),
%%  zlib:inflateInit(Z, -15),
%%  B1 = zlib:inflate(Z, Data),
%%  zlib:inflateEnd(Z),
%%  B1.

