%%%-------------------------------------------------------------------
%% @doc cloud_key_store top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(cloud_key_store_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================
start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
  ServerSup = {cloud_key_store_server_sup, {cloud_key_store_server_sup, start_link, []},
    permanent, 1000, supervisor, [cloud_key_store_server_sup]},
  ClientSup = {cloud_key_store_client_sup, {cloud_key_store_client_sup, start_link, []},
    permanent, 1000, supervisor, [cloud_key_store_client_sup]},
  Children = case cloud_key_store_utils:get_arg(start_mode) of
		     "client_mode" ->
			     erlang:display("client_mode"),
		       [ClientSup];
		     Other ->
			     erlang:display(Other),
		       [ServerSup, ClientSup]
             end,
  {ok, { {one_for_one, 0, 1}, Children} }.
%%====================================================================
%% Internal functions
%%====================================================================

